import codecs
import datetime
import os


class WeatherDataFileExport:

    DEFAULT_SEPARATOR = ','
    NEW_LINE_SEPARATOR = '\n'
    FILE_NAME_TEMPLATE = "weather_data_{current_time}.txt"

    def __init__(self, file_path_template, weather_data):
        self.file_path_template = file_path_template
        self.weather_data = weather_data

    def get_file_path(self):
        current_date_time = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

        return self.file_path_template.format(file_name=self.FILE_NAME_TEMPLATE.format(current_time=current_date_time))

    def export(self, separator=DEFAULT_SEPARATOR):
        file_path = self.get_file_path()

        if os.path.exists(file_path):
            print("File {file_path} already exists, skipping...".format(file_path=file_path))
            return
        export_file = None

        try:
            city_weather_list = self.weather_data.city_hourly_data_list
            export_file = codecs.open(file_path, 'w', "utf-8")

            for city_weather in city_weather_list:
                export_file.write(self.get_separated_string(city_weather, separator))
        finally:
            if export_file is not None:
                export_file.close()

    def get_separated_string(self, city_weather, separator):
        result_string = ""

        for daily_weather in city_weather.hourly_weather_list:
            for hourly_weather in daily_weather:
                result_string += hourly_weather.get_separated_string(separator) + self.NEW_LINE_SEPARATOR

        return result_string
