import codecs


DEFAULT_FILE_PATH_FOR_EXPORT = "C:\\projects\\missing_weather_values.txt"

missing_state_list = []
missing_direction_list = []


def add_state_if_not_exist(state_value):
    if state_value in missing_state_list:
        return

    missing_state_list.append(state_value)


def add_direction_if_not_exist(direction_value):
    if direction_value in missing_direction_list:
        return

    missing_direction_list.append(direction_value)


def export_missing_values_to_file(file_path=DEFAULT_FILE_PATH_FOR_EXPORT):
    export_file = codecs.open(file_path, "w", "utf-8")
    try:
        if len(missing_state_list) != 0:
            export_missing_values(export_file, missing_state_list, "Missing weather state values:\n")
        if len(missing_direction_list) != 0:
            export_missing_values(export_file, missing_direction_list, "Missing wind directions values:\n")
    finally:
        export_file.close()


def export_missing_values(export_file, missing_values, title):
    export_file.write(title)

    for value in missing_values:
        export_file.write(value + "\n")
