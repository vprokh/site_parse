import time
from datetime import datetime

from FileUtils.file_export import WeatherDataFileExport
from Models.city_weather_data import WeatherDataByCity
from Models.country_weather_data import CountryWeatherData
from Parser.html_beauty_soup_provider import HtmlBeautySoupProvider
from Parser.parse_data_provider import ParseDataProvider
from Parser.weather_parser import WeatherParser
from Parser.web_util import get_page, validate_page

FILE_EXPORT_PATH_TEMPLATE = "C:\\projects\\{file_name}"

BASE_URL = "https://meteo.ua/archive"
DATE_START_FROM = datetime(2003, 1, 1).date()  # the earliest date on the site
DATE_TO = datetime(2003, 1, 5).date()  # datetime.today().strftime("%Y-%m-%d")
DELAY_TIME_IN_SECONDS_WHEN_CONNECTION_EXCEPTION = 90


def run():
    country_weather = get_country_weather_data("Ukraine", ParseDataProvider(BASE_URL, DATE_START_FROM))
    WeatherDataFileExport(FILE_EXPORT_PATH_TEMPLATE, country_weather).export()


def get_country_weather_data(country, parse_data_provider):
    city_weather_list = []

    for city_code in parse_data_provider.city_codes_from_site:
        parse_data = parse_data_provider.build_parse_data(city_code)
        print("Parsing URL: {site_url}".format(site_url=parse_data.page_url))
        print("Parsing city: {parse_city}".format(parse_city=parse_data.city))

        city_weather_list.append(get_city_weather_data(parse_data))

    return CountryWeatherData(country, city_weather_list)


def get_city_weather_data(parse_data, current_weather_list=None):
    if current_weather_list is None:
        current_weather_list = []
    weather_list = current_weather_list

    try:
        while parse_data.date < DATE_TO:
            print("Parsing date: {current_parse_date}".format(current_parse_date=parse_data.date))
            page = get_page(parse_data.page_url)

            if validate_page(page):
                beauty_soup_provider = HtmlBeautySoupProvider(page)
                weather_parser = WeatherParser(beauty_soup_provider.get_beauty_soup(), parse_data)
                weather_list.append(weather_parser.parse())
            else:
                print("Can`t get requested URL: {page_url}. Skipping...".format(page_url=parse_data.page_url))

            page.close()
            parse_data.next_day()
    except Exception as parse_error:
        print("Some error just occurred\n. {exp_message} \nTrying again in {delay_time} seconds".format(
            exp_message=str(parse_error), delay_time=DELAY_TIME_IN_SECONDS_WHEN_CONNECTION_EXCEPTION))

        time.sleep(DELAY_TIME_IN_SECONDS_WHEN_CONNECTION_EXCEPTION)
        get_city_weather_data(parse_data, weather_list)

    return WeatherDataByCity(parse_data.city, weather_list)


run()
