from abc import ABC, abstractmethod


class BaseParser(ABC):

    def __init__(self, html_beauty_soup):
        self.html_beauty_soup = html_beauty_soup

    @abstractmethod
    def parse(self):
        pass
