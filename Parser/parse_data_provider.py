from Models.parse_data import ParseData


class ParseDataProvider:
    city_codes_from_site = [34, 150, 111, 164, 169, 172, 44, 134, 122, 112, 170,
                            97, 91, 83, 71, 13, 23, 28, 31, 47, 49, 56, 58, 67, 6]

    cities = {
        34: "kiev",
        150: "harkov",
        111: "odessa",
        164: "dnepr-dnepropetrovsk",
        169: "donetsk",
        172: "zaporoje",
        44: "lvov",
        134: "simferopol",
        122: "herson",
        112: "nikolaev",
        170: "lugansk",
        97: "kropivnitskiy-kirovograd",
        91: "chernovtsyi",
        83: "ujgorod",
        71: "vinnitsa",
        13: "lutsk",
        23: "sumyi",
        28: "rovno",
        31: "jitomir",
        47: "ternopol",
        49: "hmelnitskiy",
        56: "cherkassyi",
        58: "poltava",
        67: "ivano-frankovsk",
        6: "chernigov"
    }

    DEFAULT_CITY = "kiev"

    def __init__(self, base_url, date_from):
        self.base_url = base_url
        self.date_from = date_from

        self.CURRENT_DATE = date_from

    def build_parse_data(self, city_code):
        city = self.cities.get(city_code, self.DEFAULT_CITY)
        page_url = (self.base_url + "/{city_code}/{city}/{date}").format(city_code=city_code, city=city,
                                                                         date=self.CURRENT_DATE)

        return ParseData(page_url, self.date_from, city)
