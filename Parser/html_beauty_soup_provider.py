from bs4 import BeautifulSoup


class HtmlBeautySoupProvider:

    SOUP_HTML_PARSE_TYPE = "html.parser"

    def __init__(self, page):
        self.page = page

    def get_beauty_soup(self):
        beauty_soup = BeautifulSoup(self.page.text, self.SOUP_HTML_PARSE_TYPE)
        beauty_soup.encode("utf-8")

        return beauty_soup
