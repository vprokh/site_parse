from bs4 import BeautifulSoup

from Models.weather_data import WeatherData
from Models.weather_data_indexes import WeatherDataIndexes
from Models.weather_state import WeatherState
from Models.wind_data import WindData
from Models.wind_direction import WindDirection
from Parser.base_parser import BaseParser


class WeatherParser(BaseParser):
    def __init__(self, html_beauty_soup, parse_data):
        super().__init__(html_beauty_soup)
        self.parse_data = parse_data
        if isinstance(html_beauty_soup, BeautifulSoup):
            self.html_beauty_soup = html_beauty_soup
        else:
            raise ValueError("[html_beauty_soup] parameter must be type of BeautifulSoup")

    def parse(self):
        # remove first one since its for table header values
        archive_table_data_list = self.html_beauty_soup.find(class_="archive_table").find_all("tr")[1:]
        weather_data_list = []

        for tr in archive_table_data_list:
            weather_data_list.append(self.get_weather_data(tr))

        return weather_data_list

    def get_weather_data(self, tr):
        td_list = tr.find_all("td")
        weather_data_list = self.parse_weather_data(td_list)

        start_time = weather_data_list[WeatherDataIndexes.TIME_INDEX]
        weather_state = self.parse_weather_state(td_list[WeatherDataIndexes.WEATHER_STATE_INDEX])
        temperature_in_celcius = self.parse_temperature_without_celcius(
            weather_data_list[WeatherDataIndexes.TEMPERATURE_INDEX])
        wind_data = self.parse_wind_data(td_list[WeatherDataIndexes.WIND_DATA_INDEX])
        atmosphere_pressure = weather_data_list[WeatherDataIndexes.ATMOSPHERE_PRESSURE_INDEX]
        humidity = weather_data_list[WeatherDataIndexes.HUMIDITY_INDEX]

        return WeatherData(start_time, weather_state, temperature_in_celcius, wind_data, atmosphere_pressure, humidity,
                           self.parse_data.date, self.parse_data.city)

    def parse_temperature_without_celcius(self, temperature_value):
        return str(temperature_value).replace(self.parse_data.CELCIUS_SYMBOL, '')

    def parse_weather_data(self, td_list):
        data_list = []

        for td in td_list:
            data_list.append(td.find(class_="vl_child").span.string)

        return data_list

    def parse_weather_state(self, state_td):
        return WeatherState.get_weather_state(state_td.find(class_="ov_hide").span.span.string)

    def parse_wind_data(self, wind_td):
        wind_direction = WindDirection.get_wind_direction(wind_td.img["title"])
        wind_speed = wind_td.span.span.next.next  # ignoring first value since it is the image representing wind arrow

        return WindData(wind_speed, wind_direction)
