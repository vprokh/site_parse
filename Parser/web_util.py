import requests

STATUS_CODE_OK = 200


def get_page(page_url):
    return requests.get(page_url)


def validate_page(page):
    if not isinstance(page, requests.Response):
        return False

    if page.status_code != STATUS_CODE_OK:
        return False

    return True
