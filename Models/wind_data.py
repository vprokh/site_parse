from Models.wind_direction import WindDirection


class WindData:
    def __init__(self, wind_speed, wind_direction):
        self.id = None
        self.wind_speed = wind_speed
        self.wind_direction = wind_direction

    def get_separated_string(self, separator):
        return self.wind_speed + separator + self.wind_direction.value if self.wind_direction is not None \
            else WindDirection.UNKNOWN
