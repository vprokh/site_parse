import datetime


class ParseData:

    CELCIUS_SYMBOL = '°C'

    def __init__(self, page_url, date, city):
        self.page_url = page_url
        self.date = date
        self.city = city

    def next_day(self):
        self.date += datetime.timedelta(days=1)
