class CountryWeatherData:
    def __init__(self, country, city_hourly_data_list):
        self.city_hourly_data_list = city_hourly_data_list if city_hourly_data_list is not None else []
        self.country = country

    def add(self, city_weather_data):
        self.city_hourly_data_list.append(city_weather_data)
