from enum import Enum


class WindDirection(Enum):
    NORTH = "North"
    NORTHEAST = "Northeast"
    EAST = "East"
    SOUTHEAST = "Southeast"
    SOUTH = "South"
    SOUTHWEST = "Southwest"
    WEST = "West"
    NORTHWEST = "Northwest"
    UNKNOWN = "Unknown"

    @staticmethod
    def get_wind_direction(wind_direction):
        switcher = {
            "Северный": WindDirection.NORTH,
            "Северо-восточный": WindDirection.NORTHEAST,
            "Восточный": WindDirection.EAST,
            "Юго-восточный": WindDirection.SOUTHEAST,
            "Южный": WindDirection.SOUTH,
            "Юго-западный": WindDirection.SOUTHWEST,
            "Западный": WindDirection.WEST,
            "Северо-западный": WindDirection.NORTHWEST
        }

        return switcher.get(wind_direction, wind_direction)
