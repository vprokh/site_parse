from enum import Enum


class WeatherState(Enum):
    SUNNY = "Sunny"
    CLEAR = "Clear"
    RAIN = "Rain"
    LIGHT_RAIN_AND_ICE = "Light rain and ice"
    CLOUDY = "Cloudy"
    PARTLY_CLOUDY = "Partly cloudy"
    PARTLY_SUNNY = "Partly sunny"
    LIGHT_SNOW = "Light snow"
    OVERCAST = "Overcast"
    SNOW = "Snow"
    HAZE = "Haze"
    FOG = "Fog"
    HEAVY_SNOW = "Heavy snow"
    HEAVY_RAIN_WITH_SNOW = "Heavy rain with snow"
    HEAVY_RAIN = "Heavy rain"
    SNOW_AND_FOG = "Snow and fog"
    LIGHT_SNOWSTORM = "Light snowstorm"
    STRONG_SNOWSTORM = "Strong snowstorm"
    ICE = "Ice"
    UNDEFINED = "Undefined"

    @staticmethod
    def get_weather_state(state):
        switcher = {
            "Слабый снег": WeatherState.LIGHT_SNOW,
            "Сплошная облачность": WeatherState.OVERCAST,
            "Небольшая облачность": WeatherState.OVERCAST,
            "Снег": WeatherState.SNOW,
            "Дымка": WeatherState.HAZE,
            "Туман": WeatherState.FOG,
            "Ясно": WeatherState.SUNNY,
            "Переменная облачность": WeatherState.PARTLY_CLOUDY,
            "Сильный снег": WeatherState.HEAVY_SNOW,
            "Ливневый дождь со снегом": WeatherState.HEAVY_RAIN_WITH_SNOW,
            "Ливневый дождь": WeatherState.HEAVY_RAIN,
            "Снег, туман": WeatherState.SNOW_AND_FOG,
            "Слабый дождь, гололед": WeatherState.LIGHT_RAIN_AND_ICE,
            "Слабая метель": WeatherState.LIGHT_SNOWSTORM,
            "Сильная метель": WeatherState.STRONG_SNOWSTORM,
            "Гололед": WeatherState.ICE
        }

        return switcher.get(state, state)
