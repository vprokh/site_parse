class WeatherDataByCity:
    def __init__(self, city, hourly_weather_list):
        self.hourly_weather_list = hourly_weather_list if hourly_weather_list is not None else []
        self.city = city

    def add(self, hourly_weather):
        self.hourly_weather_list.append(hourly_weather)
