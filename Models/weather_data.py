from Models.weather_state import WeatherState


class WeatherData:
    def __init__(self, start_time=None, weather_state=None, temperature_in_celcius=None,
                 wind_data=None, atmosphere_pressure=None, humidity_in_percent=None, date=None, city=None):
        self.id = None
        self.start_time = start_time
        self.weather_state = weather_state
        self.temperature_in_celcius = temperature_in_celcius
        self.wind_data = wind_data
        self.atmosphere_pressure = atmosphere_pressure
        self.humidity_in_percent = humidity_in_percent
        self.date = date
        self.city = city

    def get_separated_string(self, separator):
        return str(self.date) + separator + self.start_time + separator \
               + (self.weather_state.value if isinstance(self.weather_state, WeatherState) else self.weather_state) \
               + separator + self.temperature_in_celcius + separator + self.wind_data.get_separated_string(separator) \
               + separator + self.atmosphere_pressure + separator + self.humidity_in_percent + separator + self.city
