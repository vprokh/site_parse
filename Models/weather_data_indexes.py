from enum import IntEnum


class WeatherDataIndexes(IntEnum):
    TIME_INDEX = 0
    WEATHER_STATE_INDEX = 1
    TEMPERATURE_INDEX = 2
    WIND_DATA_INDEX = 3
    ATMOSPHERE_PRESSURE_INDEX = 4
    HUMIDITY_INDEX = 5
