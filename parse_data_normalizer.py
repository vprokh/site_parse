import csv
import os

WEATHER_DATA_FILES_DIR_PATH = "C:\\projects\\meteo_station\\parsed_data"
TEMPERATURE_INDEX_COLUMN = 3
DATA_VALUES = []
RESULT_FILE = WEATHER_DATA_FILES_DIR_PATH + "\\result.csv"


def find_filenames(path_to_dir, suffix=".csv"):
    file_list = []
    directory_item_list = os.listdir(path_to_dir)

    for dir_item in directory_item_list:
        full_item_path = path_to_dir + "\\" + dir_item

        if os.path.isfile(full_item_path) and dir_item.endswith(suffix):
            file_list.append(full_item_path)
        elif not os.path.isfile(full_item_path):
            file_list.extend(find_filenames(full_item_path))

    return file_list


def fill_correct_data(row):
    correct_temperature = row[TEMPERATURE_INDEX_COLUMN + 1]
    correct_wind_speed = row[TEMPERATURE_INDEX_COLUMN + 2]
    correct_wind_direction = row[TEMPERATURE_INDEX_COLUMN + 3]
    correct_pressure = row[TEMPERATURE_INDEX_COLUMN + 4]
    correct_humidity = row[TEMPERATURE_INDEX_COLUMN + 5]
    correct_city = row[TEMPERATURE_INDEX_COLUMN + 6]

    row[TEMPERATURE_INDEX_COLUMN] = correct_temperature
    row[TEMPERATURE_INDEX_COLUMN + 1] = correct_wind_speed
    row[TEMPERATURE_INDEX_COLUMN + 2] = correct_wind_direction
    row[TEMPERATURE_INDEX_COLUMN + 3] = correct_pressure
    row[TEMPERATURE_INDEX_COLUMN + 4] = correct_humidity
    row[TEMPERATURE_INDEX_COLUMN + 5] = correct_city


def get_values_to_override(file_name):
    values_to_override = {}
    DATA_VALUES.clear()

    with open(file_name, 'r', encoding='UTF-8') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        DATA_VALUES.extend(csv_reader)

        for line_num, row in enumerate(DATA_VALUES):
            temperature_value = row[TEMPERATURE_INDEX_COLUMN]

            if not is_digit(temperature_value):
                fill_correct_data(row)
                values_to_override[line_num] = row

    return values_to_override


def process_copy(file_name, values_to_override):
    if len(values_to_override) == 0:
        return

    print("Processing file copy for {file_name}".format(file_name=file_name))
    with open(file_name, 'w', encoding="utf-8", newline='') as csv_file:
        writer = csv.writer(csv_file)

        for line_num, row in enumerate(DATA_VALUES):
            data = values_to_override.get(line_num)
            if data is not None:
                DATA_VALUES[line_num] = data

        for data_value in DATA_VALUES:
            writer.writerow(data_value)


def get_row_without_redundant(row):
    row[2] = row[3]
    row[3] = row[4]
    row[4] = row[6]
    row[5] = row[7]
    row[6] = row[8]
    row[7] = ''
    row[8] = ''

    return row


def delete_redundant_columns(file_name):
    file_content = []

    with open(file_name, 'r', encoding='UTF-8') as csv_file:
        file_content.extend(csv.reader(csv_file))

        for line_num, row in enumerate(file_content):
            if len(row) >= 9:
                file_content[line_num] = get_row_without_redundant(row)

    with open(file_name, 'w', encoding="utf-8", newline='') as csv_file:
        writer = csv.writer(csv_file)

        for row in file_content:
            writer.writerow(row)


def normalize_data(file_name):
    values_to_override = get_values_to_override(file_name)
    process_copy(file_name, values_to_override)
    delete_redundant_columns(file_name)


def run_normalization(dir_path):
    filenames = find_filenames(dir_path)

    for file_name in filenames:
        normalize_data(file_name)


def is_digit(n):
    try:
        int(n)
        return True
    except ValueError:
        return False


def merge_files(file_name, result_file_path):
    file_content = []

    with open(file_name, 'r', encoding="UTF-8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        file_content.extend(csv_reader)

    with open(result_file_path, 'a', encoding="utf-8", newline='') as csv_file:
        writer = csv.writer(csv_file)

        for row in file_content:
            writer.writerow(row)


def run_file_merging(dir_path, result_file_path):
    for file_name in find_filenames(dir_path):
        merge_files(file_name, result_file_path)


if __name__ == '__main__':
    run_normalization(WEATHER_DATA_FILES_DIR_PATH)
    run_file_merging(WEATHER_DATA_FILES_DIR_PATH, RESULT_FILE)
